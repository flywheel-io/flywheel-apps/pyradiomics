import pytest
import SimpleITK as sitk

from fw_gear_pyradiomics.imgreaders import (
    DicomZipImageReader,
    EmptyMaskImageReader,
    ImageReader,
    NumpyImageReader,
    SimpleITKImageReader,
)


@pytest.mark.parametrize(
    "path_image, image_reader_class",
    [
        ("tests/assets/chest_xray.png", SimpleITKImageReader),
        ("tests/assets/chest_xray.npy", NumpyImageReader),
        (
            "tests/assets/9189822998 - 5865 - Surview Test.dicom.zip",
            DicomZipImageReader,
        ),
        ("", EmptyMaskImageReader),
    ],
)
def test_image_readers(path_image, image_reader_class):
    image_reader = ImageReader.factory(path_image=path_image)
    assert isinstance(image_reader, image_reader_class)
    assert isinstance(image_reader.read_image(), sitk.Image)


def test_image_reader_wrong_file_type(tmpdir):
    """Test that the ImageReader factory throws an error if we try to
    use an existing file not supported by the gear
    """

    # create empty text file
    dummy_file_path = tmpdir / "foo.txt"
    with open(dummy_file_path, "w") as dummy:
        print(dummy)
        pass

    with pytest.raises(
        ValueError,
        match=f"Could not find instantiation of class {ImageReader.__name__}",
    ) as factory_error:
        # with pytest.raises(Exception) as factory_error:
        # try to create a reader for a file that exists, but it is not
        # an image supported by the gear (e.g., a plain text file):
        ImageReader.factory(path_image=dummy_file_path)
        assert "instantiation of class" in str(factory_error.value)
