import csv
import logging
from collections import OrderedDict
from pathlib import Path
from unittest.mock import MagicMock

import pytest
from flywheel_gear_toolkit import GearToolkitContext

from fw_gear_pyradiomics.pyradwriters import PyradFlywheelWriter
from fw_gear_pyradiomics.writers import (
    CsvFlywheelWriter,
    FileInfoFlywheelHandler,
    GearInfoFlywheelHandler,
)
from tests.test_imgprep import config_dicts

log = logging.getLogger(__name__)
log.setLevel(logging.DEBUG)

# Features derived from zipped dicom w/ 5 slices
features = OrderedDict(
    [
        (
            "diagnostics_Versions_PyRadiomics",
            ["v3.0.1", "v3.0.1", "v3.0.1", "v3.0.1", "v3.0.1"],
        ),
        (
            "diagnostics_Versions_Numpy",
            ["1.22.3", "1.22.3", "1.22.3", "1.22.3", "1.22.3"],
        ),
        (
            "diagnostics_Versions_SimpleITK",
            ["2.1.1.2", "2.1.1.2", "2.1.1.2", "2.1.1.2", "2.1.1.2"],
        ),
        (
            "diagnostics_Versions_PyWavelet",
            ["1.3.0", "1.3.0", "1.3.0", "1.3.0", "1.3.0"],
        ),
        ("diagnostics_Versions_Python", ["3.9.7", "3.9.7", "3.9.7", "3.9.7", "3.9.7"]),
        (
            "diagnostics_Configuration_Settings",
            [
                {
                    "minimumROIDimensions": 2,
                    "minimumROISize": None,
                    "normalize": False,
                    "normalizeScale": 1,
                    "removeOutliers": None,
                    "resampledPixelSpacing": None,
                    "interpolator": "sitkBSpline",
                    "preCrop": False,
                    "padDistance": 5,
                    "distances": [1],
                    "force2D": False,
                    "force2Ddimension": 0,
                    "resegmentRange": None,
                    "label": 1,
                    "additionalInfo": True,
                },
                {
                    "minimumROIDimensions": 2,
                    "minimumROISize": None,
                    "normalize": False,
                    "normalizeScale": 1,
                    "removeOutliers": None,
                    "resampledPixelSpacing": None,
                    "interpolator": "sitkBSpline",
                    "preCrop": False,
                    "padDistance": 5,
                    "distances": [1],
                    "force2D": False,
                    "force2Ddimension": 0,
                    "resegmentRange": None,
                    "label": 1,
                    "additionalInfo": True,
                },
                {
                    "minimumROIDimensions": 2,
                    "minimumROISize": None,
                    "normalize": False,
                    "normalizeScale": 1,
                    "removeOutliers": None,
                    "resampledPixelSpacing": None,
                    "interpolator": "sitkBSpline",
                    "preCrop": False,
                    "padDistance": 5,
                    "distances": [1],
                    "force2D": False,
                    "force2Ddimension": 0,
                    "resegmentRange": None,
                    "label": 1,
                    "additionalInfo": True,
                },
                {
                    "minimumROIDimensions": 2,
                    "minimumROISize": None,
                    "normalize": False,
                    "normalizeScale": 1,
                    "removeOutliers": None,
                    "resampledPixelSpacing": None,
                    "interpolator": "sitkBSpline",
                    "preCrop": False,
                    "padDistance": 5,
                    "distances": [1],
                    "force2D": False,
                    "force2Ddimension": 0,
                    "resegmentRange": None,
                    "label": 1,
                    "additionalInfo": True,
                },
                {
                    "minimumROIDimensions": 2,
                    "minimumROISize": None,
                    "normalize": False,
                    "normalizeScale": 1,
                    "removeOutliers": None,
                    "resampledPixelSpacing": None,
                    "interpolator": "sitkBSpline",
                    "preCrop": False,
                    "padDistance": 5,
                    "distances": [1],
                    "force2D": False,
                    "force2Ddimension": 0,
                    "resegmentRange": None,
                    "label": 1,
                    "additionalInfo": True,
                },
            ],
        ),
        (
            "diagnostics_Configuration_EnabledImageTypes",
            [
                {"Original": {}},
                {"Original": {}},
                {"Original": {}},
                {"Original": {}},
                {"Original": {}},
            ],
        ),
        (
            "diagnostics_Image-original_Hash",
            [
                "3357830cacef2549cff54235656a011ee6e33149",
                "3357830cacef2549cff54235656a011ee6e33149",
                "3357830cacef2549cff54235656a011ee6e33149",
                "3357830cacef2549cff54235656a011ee6e33149",
                "3357830cacef2549cff54235656a011ee6e33149",
            ],
        ),
        ("diagnostics_Image-original_Dimensionality", ["2D", "2D", "2D", "2D", "2D"]),
        (
            "diagnostics_Image-original_Spacing",
            [
                (0.9765625, 0.9765625),
                (0.9765625, 0.9765625),
                (0.9765625, 0.9765625),
                (0.9765625, 0.9765625),
                (0.9765625, 0.9765625),
            ],
        ),
        (
            "diagnostics_Image-original_Size",
            [(512, 665), (512, 665), (512, 665), (512, 665), (512, 665)],
        ),
        (
            "diagnostics_Image-original_Mean",
            [
                -72.24022556390977,
                -72.24022556390977,
                -72.24022556390977,
                -72.24022556390977,
                -72.24022556390977,
            ],
        ),
        (
            "diagnostics_Image-original_Minimum",
            [-1000.0, -1000.0, -1000.0, -1000.0, -1000.0],
        ),
        (
            "diagnostics_Image-original_Maximum",
            [1092.0, 1092.0, 1092.0, 1092.0, 1092.0],
        ),
        (
            "diagnostics_Mask-original_Hash",
            [
                "a080361cabddfbc0e7cc4e065fec014de4e950a5",
                "a080361cabddfbc0e7cc4e065fec014de4e950a5",
                "a080361cabddfbc0e7cc4e065fec014de4e950a5",
                "a080361cabddfbc0e7cc4e065fec014de4e950a5",
                "a080361cabddfbc0e7cc4e065fec014de4e950a5",
            ],
        ),
        (
            "diagnostics_Mask-original_Spacing",
            [
                (0.9765625, 0.9765625),
                (0.9765625, 0.9765625),
                (0.9765625, 0.9765625),
                (0.9765625, 0.9765625),
                (0.9765625, 0.9765625),
            ],
        ),
    ]
)

features_chest_xray = OrderedDict(
    [
        ("diagnostics_Versions_PyRadiomics", "v3.0.1"),
        ("diagnostics_Versions_Numpy", "1.22.3"),
        ("diagnostics_Versions_SimpleITK", "2.1.1.2"),
        ("diagnostics_Versions_PyWavelet", "1.3.0"),
        ("diagnostics_Versions_Python", "3.9.7"),
        (
            "diagnostics_Configuration_Settings",
            {
                "minimumROIDimensions": 2,
                "minimumROISize": None,
                "normalize": False,
                "normalizeScale": 1,
                "removeOutliers": None,
                "resampledPixelSpacing": None,
                "interpolator": "sitkBSpline",
                "preCrop": False,
                "padDistance": 5,
                "distances": [1],
                "force2D": False,
                "force2Ddimension": 0,
                "resegmentRange": None,
                "label": 1,
                "additionalInfo": True,
            },
        ),
        ("diagnostics_Configuration_EnabledImageTypes", {"Original": {}}),
        ("diagnostics_Image-original_Hash", "86ff7b6299a905f1ce6c0c2124a5bda97b1d4615"),
        ("diagnostics_Image-original_Dimensionality", "2D"),
        ("diagnostics_Image-original_Spacing", (1.0, 1.0)),
        ("diagnostics_Image-original_Size", (1015, 1100)),
        ("diagnostics_Image-original_Mean", 148.6433103448276),
        ("diagnostics_Image-original_Minimum", 0.0),
        ("diagnostics_Image-original_Maximum", 255.0),
        ("diagnostics_Mask-original_Hash", "4385a9cf49936d5548d99b407336a3accec48c39"),
        ("diagnostics_Mask-original_Spacing", (1.0, 1.0)),
    ]
)

header_expected = list(features.keys())
row_expected = [
    "v3.0.1",
    "1.22.3",
    "2.1.1.2",
    "1.3.0",
    "3.9.7",
    "{'minimumROIDimensions': 2, 'minimumROISize': None, 'normalize': False, 'normalizeScale': 1, 'removeOutliers': None, 'resampledPixelSpacing': None, 'interpolator': 'sitkBSpline', 'preCrop': False, 'padDistance': 5, 'distances': [1], 'force2D': False, 'force2Ddimension': 0, 'resegmentRange': None, 'label': 1, 'additionalInfo': True}",
    "{'Original': {}}",
    "3357830cacef2549cff54235656a011ee6e33149",
    "2D",
    "(0.9765625, 0.9765625)",
    "(512, 665)",
    "-72.24022556390977",
    "-1000.0",
    "1092.0",
    "a080361cabddfbc0e7cc4e065fec014de4e950a5",
    "(0.9765625, 0.9765625)",
]
header_expected_chest_xray = list(features_chest_xray.keys())
row_expected_chest_xray = [
    "v3.0.1",
    "1.22.3",
    "2.1.1.2",
    "1.3.0",
    "3.9.7",
    "{'minimumROIDimensions': 2, 'minimumROISize': None, 'normalize': False, 'normalizeScale': 1, 'removeOutliers': None, 'resampledPixelSpacing': None, 'interpolator': 'sitkBSpline', 'preCrop': False, 'padDistance': 5, 'distances': [1], 'force2D': False, 'force2Ddimension': 0, 'resegmentRange': None, 'label': 1, 'additionalInfo': True}",
    "{'Original': {}}",
    "86ff7b6299a905f1ce6c0c2124a5bda97b1d4615",
    "2D",
    "(1.0, 1.0)",
    "(1015, 1100)",
    "148.6433103448276",
    "0.0",
    "255.0",
    "4385a9cf49936d5548d99b407336a3accec48c39",
    "(1.0, 1.0)",
]


@pytest.mark.parametrize(
    "image, mask, features_test, header_expected_test, row_expected_test",
    [
        (
            "9189822998 - 5865 - Surview Test.dicom.zip",
            "9189822998 - 5865 - Surview Test_mask.nrrd",
            features,
            header_expected,
            row_expected,
        ),
        (
            "chest_xray.png",
            "chest_xray_mask.nrrd",
            features_chest_xray,
            header_expected_chest_xray,
            row_expected_chest_xray,
        ),
    ],
)
def test_pyrad_flywheel_writer(
    tmpdir, image, mask, features_test, header_expected_test, row_expected_test
):
    gear_context = MagicMock(spec=GearToolkitContext)
    config_dict = config_dicts[0]

    def side_effect(input_val):
        if input_val == "image":
            return image
        elif input_val == "mask":
            return mask
        else:
            raise ValueError(
                "input val can only be 'image' or 'mask', " "not '%s'" % input_val
            )

    # prepare our mocker object with mock functions
    gear_context.get_input_filename = side_effect
    output_dir = Path(tmpdir.mkdir("output"))
    gear_context.output_dir = output_dir
    gear_context.config = config_dict

    # Get writer based on feature type
    pyrad_writer = PyradFlywheelWriter.factory(
        gear_context=gear_context, features=features_test
    )
    path_out_csv = pyrad_writer.write_features_to_csv(input_name="image")

    # Check output csv
    with open(path_out_csv, "r") as in_csv:
        csvreader = csv.reader(in_csv)
        assert next(csvreader) == header_expected_test
        assert next(csvreader) == row_expected_test


def test_get_input_file_info_success(mocker):
    # Mock GearToolkitContext
    mock_gear_context = mocker.Mock(spec=GearToolkitContext)

    # Mock the get_input method to return a valid file info
    mock_gear_context.get_input.return_value = {"object": {"info": {"some": "info"}}}

    # Mock the manifest with the input name
    mock_gear_context.manifest = {"inputs": {"input_image": {}}}

    handler = FileInfoFlywheelHandler(gear_context=mock_gear_context)
    input_name = "input_image"

    # Call the method
    result = handler.get_input_file_info(input_name=input_name)

    # Assert the result is as expected
    assert result == {"some": "info"}


def test_get_input_file_info_not_found(mocker):
    # Mock GearToolkitContext
    mock_gear_context = mocker.Mock(spec=GearToolkitContext)

    # Mock the get_input method to return None
    mock_gear_context.get_input.return_value = None

    # Mock the manifest with some inputs
    mock_gear_context.manifest = {"inputs": {"input_image": {}}}

    handler = FileInfoFlywheelHandler(gear_context=mock_gear_context)
    input_name = "input_image"

    # Call the method and assert it raises ValueError
    with pytest.raises(
        ValueError,
        match=r"input_name 'input_image' was not found in context. Only the following\s*are available:\s*\['input_image'\]",
    ):
        handler.get_input_file_info(input_name=input_name)


def test_get_gear_dict_from_file(mocker):
    # Mock the gear context
    mock_gear_context = mocker.Mock()
    # Mock the file info returned by get_input_file_info
    mock_file_info = {
        "gears": {
            "gear_name": {
                "gear_info": {"name": "gear_name", "version": "1.0", "config": {}}
            }
        }
    }
    # Patch the get_input_file_info method to return the mock file info
    mocker.patch.object(
        FileInfoFlywheelHandler, "get_input_file_info", return_value=mock_file_info
    )

    handler = FileInfoFlywheelHandler(gear_context=mock_gear_context)
    gear_dict = handler.get_gear_dict_from_file(input_name="input_image")

    assert gear_dict is not None
    assert isinstance(gear_dict, dict)
    assert "gear_name" in gear_dict
    assert isinstance(gear_dict["gear_name"], dict)
    assert "gear_info" in gear_dict["gear_name"]
    assert gear_dict["gear_name"]["gear_info"]["name"] == "gear_name"
    assert gear_dict["gear_name"]["gear_info"]["version"] == "1.0"


@pytest.fixture
def gear_context_mock():
    # Create a MagicMock object to mock GearToolkitContext
    return MagicMock()


@pytest.fixture
def csv_writer():
    csv_writer_mock = MagicMock(spec=CsvFlywheelWriter)
    # Mock the behavior of the abstract method
    csv_writer_mock.write_features_to_csv.side_effect = NotImplementedError(
        "Mocked method"
    )
    return csv_writer_mock


def test_write_features_to_csv(csv_writer):
    # Define test data
    features = {"feature1": 1, "feature2": 2, "feature3": 3}
    # Call the method to be tested
    with pytest.raises(NotImplementedError):
        csv_writer.write_features_to_csv(features)


def test_write_features_to_csv_with_image(csv_writer):
    image = "mock image"
    with pytest.raises(NotImplementedError):
        csv_writer.write_features_to_csv(image)


def test_get_gear_version(mocker, gear_context_mock):
    # Set up mock gear context
    gear_context_mock.manifest = {"version": "1.0"}
    # Patching the GearToolkitContext to return the mock
    mocker.patch(
        "flywheel_gear_toolkit.GearToolkitContext", return_value=gear_context_mock
    )
    # Instantiate GearInfoFlywheelHandler with the mock gear context
    handler = GearInfoFlywheelHandler(gear_context=gear_context_mock)
    # Perform the test
    assert handler.get_gear_version() == "1.0"


def test_create_base_gear_dict(gear_context_mock):
    # Set up mock gear context
    gear_context_mock.manifest = {"version": "1.0"}
    # Instantiate GearInfoFlywheelHandler with the mock gear context
    handler = GearInfoFlywheelHandler(gear_context=gear_context_mock)
    # Perform the test
    assert handler.get_gear_version() == "1.0"
