"""Module to test main.py"""
import logging
from collections import OrderedDict
from unittest import mock

import SimpleITK as sitk

from fw_gear_pyradiomics.main import run
from fw_gear_pyradiomics.pyrad import PyradFactory

logging.basicConfig()
log = logging.getLogger()
log.setLevel(logging.DEBUG)


# @pytest.mark.parametrize(
#     "path_image, path_mask, key_check, val_check, config_dict",
#     [
#         (  # Test SimpleITK with mask
#             "tests/assets/chest_xray.png",
#             "tests/assets/chest_xray_mask.nrrd",
#             "original_shape2D_MeshSurface",
#             14999.5,
#             config_dicts[0],
#         ),
#         (  # Test SimpleITK without mask
#             "tests/assets/chest_xray.png",
#             "",
#             "original_shape2D_MeshSurface",
#             None,
#             config_dicts[0],
#         ),
#         (  # test zipped dicom and its mask
#             "tests/assets/9189822998 - 5865 - Surview Test.dicom.zip",
#             "tests/assets/9189822998 - 5865 - Surview Test_mask.nrrd",
#             "original_shape2D_MeshSurface",
#             [
#                 np.array(324705.6007385254),
#                 np.array(324705.6007385254),
#                 np.array(324705.6007385254),
#                 np.array(324705.6007385254),
#                 np.array(324705.6007385254),
#             ],
#             config_dicts[0],
#         ),
#         (  # test dicom and an empty mask
#             "tests/assets/9189822998 - 5865 - Surview Test.dicom.zip",
#             "",
#             "original_shape2D_MeshSurface",
#             None,
#             config_dicts[0],
#         ),
#     ],
# )
# def test_run(
#     path_image: (str, os.PathLike),
#     path_mask: (str, os.PathLike),
#     key_check: str,
#     val_check,
#     config_dict: dict,
# ):
#     """
#     Test 2D and 3D images with and without mask passed.
#
#     Note: this will fail if voxel-based features are calculated. Create a
#     seaprate test for voxel-based features.
#     """
#     with tempfile.TemporaryDirectory() as tempdir:
#         pyrad_factory = PyradFactory(
#             PyradFactoryParams(
#                 path_image=path_image,
#                 path_mask=path_mask,
#                 output_dir=tempdir,
#                 config_dict=config_dict,
#             )
#         )
#         err_code, features = run(pyrad_factory)
#
#         # Assert that the error code is 0
#         assert err_code == 0
#
#         # Assert that the features are None
#         assert features is None
#         # assert features.get(key_check) == features.get(key_check)
#         # assert features.get(key_check) == val_check


def test_run(mocker):
    # Mock PyradFactory and its calc_radiomics method
    mock_pyrad_factory = mocker.Mock(spec=PyradFactory)

    # Define a mock return value for calc_radiomics
    mock_features = OrderedDict({"feature1": 1.0, "feature2": 2.0})
    mock_pyrad_factory.calc_radiomics.return_value = mock_features

    # Call the run function
    e_code, features = run(pyrad_factory=mock_pyrad_factory)

    # Assert the results
    assert e_code == 0
    # assert features == mock_features

    mock_image_reader = mock.Mock()
    mock_image_reader_mask = mock.Mock()
    mock_image_preparer = mock.Mock()
    mock_image_preparer_mask = mock.Mock()

    mock_image = sitk.Image(10, 10, sitk.sitkFloat32)
    mock_mask = sitk.Image(10, 10, sitk.sitkUInt8)

    mock_image_reader.read_image.return_value = mock_image
    mock_image_reader_mask.read_image.return_value = mock_mask
    mock_image_preparer.prepare_image.return_value = mock_image
    mock_image_preparer_mask.prepare_image.return_value = mock_mask

    mock_pyrad_factory.image_reader = mock_image_reader
    mock_pyrad_factory.image_reader_mask = mock_image_reader_mask
    mock_pyrad_factory.image_preparer = mock_image_preparer
    mock_pyrad_factory.image_preparer_mask = mock_image_preparer_mask

    features = mock_pyrad_factory.calc_radiomics(
        image_sitk=mock_image, mask_sitk=mock_mask
    )

    mock_pyrad_factory.calc_radiomics.assert_called_with(
        image_sitk=mock_image, mask_sitk=mock_mask
    )

    assert isinstance(features, OrderedDict)


def test_run_voxel_based(mocker):
    # Mock PyradFactory and its calc_radiomics method
    mock_pyrad_factory = mocker.Mock(spec=PyradFactory)

    # Define a mock return value for calc_radiomics with a SimpleITK image
    mock_image = sitk.Image([10, 10], sitk.sitkFloat32)
    mock_pyrad_factory.calc_radiomics.return_value = mock_image

    # Run the function
    exit_code, features = run(mock_pyrad_factory)

    # Assert the results
    assert exit_code == 0
    assert isinstance(features, sitk.Image)


def test_run_segment_based(mocker):
    # Mock PyradFactory and its calc_radiomics method
    mock_pyrad_factory = mocker.Mock(spec=PyradFactory)

    # Define a mock return value for calc_radiomics with an OrderedDict
    mock_features = OrderedDict({"feature1": 1.0, "feature2": 2.0})
    mock_pyrad_factory.calc_radiomics.return_value = mock_features

    # Run the function
    exit_code, features = run(mock_pyrad_factory)

    # Assert the results
    assert exit_code == 0
    assert isinstance(features, OrderedDict)
    assert features == mock_features
