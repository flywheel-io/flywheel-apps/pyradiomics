"""Module to test utils.py"""
import logging
from unittest.mock import mock_open, patch

import yaml

from fw_gear_pyradiomics.utils import (
    load_parameters,
    validate_parameters_file,
    validate_required_sections,
    validate_setting,
)

logging.basicConfig()
log = logging.getLogger()
log.setLevel(logging.DEBUG)


# Define test functions
@patch("builtins.open")
@patch("yaml.safe_load")
def test_validate_parameters_file(mock_yaml_load, mock_open):
    # Mocking the YAML file content
    mock_yaml_load.return_value = {
        "setting": {"param1": 1, "param2": 2},
        "imageType": {"type1": "settings1", "type2": "settings2"},
        "featureClass": {"class1": ["feature1", "feature2"], "class2": []},
    }

    assert validate_parameters_file("dummy_path.yaml") is True
    assert validate_parameters_file(mock_yaml_load) is True

    mock_yaml_load.side_effect = yaml.YAMLError()
    assert validate_parameters_file("dummy_path.yaml") is False

    with patch("builtins.open", mock_open(read_data="")):
        # Test with an empty file
        assert validate_parameters_file("empty_file.yaml") is False

        # Test with missing sections
        mock_yaml_load.return_value = {"setting": {"param1": 1}}
        assert validate_parameters_file("missing_imageType.yaml") is False

        mock_yaml_load.return_value = {
            "setting": {"param1": 1},
            "imageType": {"type1": "settings1"},
        }
        assert validate_parameters_file("missing_featureClass.yaml") is False

        # Test with invalid values
        mock_yaml_load.return_value = {
            "setting": {"param1": 0, "param2": 2},
            "imageType": {"type1": None, "type2": "settings2"},
            "featureClass": {"class1": ["feature1", "feature2"], "class2": []},
        }
        assert validate_parameters_file("invalid_values.yaml") is False

        # Test with edge cases
        # Empty values should result in False
        mock_yaml_load.return_value = {
            "setting": {},
            "imageType": {},
            "featureClass": {},
        }
        assert validate_parameters_file("empty_values.yaml") is False

        # Empty features should result in False
        mock_yaml_load.return_value = {
            "setting": {"param1": 1, "param2": 2},
            "imageType": {"type1": "settings1", "type2": "settings2"},
            "featureClass": {"class1": [], "class2": []},
        }
        assert validate_parameters_file("empty_features.yaml") is False


def test_load_parameters_valid_file():
    yaml_content = """
    setting:
      key1: value1
    imageType:
      type1: value1
    featureClass:
      class1: [feature1, feature2]
    """
    with patch("builtins.open", mock_open(read_data=yaml_content)):
        parameters = load_parameters("dummy_path")
        assert parameters == {
            "setting": {"key1": "value1"},
            "imageType": {"type1": "value1"},
            "featureClass": {"class1": ["feature1", "feature2"]},
        }


def test_load_parameters_invalid_file():
    with patch("builtins.open", mock_open(read_data=": invalid yaml")):
        with patch("yaml.safe_load", side_effect=yaml.YAMLError):
            parameters = load_parameters("dummy_path")
            assert parameters is None


def test_validate_required_sections():
    parameters = {"setting": {}, "imageType": {}, "featureClass": {}}
    assert validate_required_sections(parameters) is True

    parameters_missing = {"setting": {}, "imageType": {}}
    assert validate_required_sections(parameters_missing) is False


def test_validate_setting():
    valid_setting = {"key1": 1, "key2": 2}
    invalid_setting = {"key1": 1, "key2": 0}
    assert validate_setting(valid_setting) is True
    assert validate_setting(invalid_setting) is False
