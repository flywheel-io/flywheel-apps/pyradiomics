from collections import OrderedDict
from unittest.mock import MagicMock

import pytest
import SimpleITK as sitk

from fw_gear_pyradiomics.pyrad import PyradCalc
from fw_gear_pyradiomics.pyradcalc import RadPyradCalc, VoxelPyradCalc, get_pyrad_calc


def test_get_pyrad_calc():
    assert get_pyrad_calc("voxel_rad_calc") == VoxelPyradCalc
    assert get_pyrad_calc("rad_calc") == RadPyradCalc
    with pytest.raises(ValueError):
        get_pyrad_calc("nonexistent_calc")


def test_pyradcalc_factory(mocker):
    params = PyradCalc.PyradCalcParams(
        pyrad_calc_name="voxel_rad_calc",
        mask_passed=True,
        path_image="path/to/image",
        path_mask="path/to/mask",
        path_params=None,
    )
    pyrad_calc = PyradCalc.factory(params)
    assert isinstance(pyrad_calc, VoxelPyradCalc)


def test_pyradcalc_check_image_dimensions():
    image = sitk.Image([10, 10], sitk.sitkFloat32)
    mask = sitk.Image([10, 10], sitk.sitkUInt8)
    PyradCalc.check_image_dimensions(image, mask)

    mask_wrong_size = sitk.Image([8, 8], sitk.sitkUInt8)
    with pytest.raises(AttributeError):
        PyradCalc.check_image_dimensions(image, mask_wrong_size)


def test_radpyradcalc_create_extractor(mocker):
    path_params = "path/to/params.yaml"

    extractor_mock = mocker.patch(
        "radiomics.featureextractor.RadiomicsFeatureExtractor", return_value=MagicMock()
    )

    rad_calc = RadPyradCalc(
        mask_passed=True,
        path_image="path/to/image",
        path_mask="path/to/mask",
        path_params=path_params,
    )

    extractor = rad_calc.create_extractor(image_sitk=MagicMock())
    assert extractor_mock.called_with(path_params)
    assert extractor_mock.called_with(image_sitk=MagicMock())
    assert extractor.enableFeatureClassByName.called


def test_radpyradcalc_calc_radiomics(mocker):
    image = sitk.Image([10, 10], sitk.sitkFloat32)
    mask = sitk.Image([10, 10], sitk.sitkUInt8)

    rad_calc = RadPyradCalc(
        mask_passed=True,
        path_image="path/to/image",
        path_mask="path/to/mask",
        path_params=None,
    )

    extractor_mock = mocker.patch.object(
        rad_calc, "create_extractor", return_value=MagicMock()
    )
    extractor_mock.return_value.execute.return_value = {
        "feature1": 1.0,
        "feature2": 2.0,
    }

    features = rad_calc.calc_radiomics(image_sitk=image, mask_sitk=mask)

    assert isinstance(features, OrderedDict)
    assert "path_image" in features
    assert "path_mask" in features
    assert "feature1" in features
    assert features["feature1"] == 1.0
    assert extractor_mock.called_with(image, mask)

    # Adding test for wrong attributes
    with pytest.raises(AttributeError) as excinfo:
        rad_calc.calc_radiomics(image_sitk="not_an_image", mask_sitk=sitk.Image())
    assert (
        str(excinfo.value)
        == "Wrong type for image_sitk. Expected SimpleITK.Image, got <class 'str'>"
    )

    image_sitk = sitk.Image([10, 10], sitk.sitkFloat32)
    mask_sitk = sitk.Image([0, 0], sitk.sitkUInt8)  # Example mask with size mismatch
    with pytest.raises(AttributeError) as excinfo:
        rad_calc.calc_radiomics(image_sitk=image_sitk, mask_sitk=mask_sitk)
    assert str(excinfo.value) == (
        "Size mismatch: the input image is size '(10, 10)' and its mask is '(0, 0)'. "
        "Sizes must be the same."
    )


def test_voxelpyradcalc_create_extractor(mocker):
    voxel_calc = VoxelPyradCalc(
        mask_passed=True,
        path_image="path/to/image",
        path_mask="path/to/mask",
        path_params=None,
    )
    extractor = voxel_calc.create_extractor(image_sitk=MagicMock())
    assert extractor is None


def test_voxelpyradcalc_calc_radiomics(mocker):
    voxel_calc = VoxelPyradCalc(
        mask_passed=True,
        path_image="path/to/image",
        path_mask="path/to/mask",
        path_params=None,
    )
    features = voxel_calc.calc_radiomics(image_sitk=MagicMock(), mask_sitk=MagicMock())
    assert features is None
