"""Main module."""
import logging

from fw_gear_pyradiomics.pyrad import PyradFactory

log = logging.getLogger(__name__)


def run(pyrad_factory: PyradFactory):
    """Calculates radiomic features using the pyradiomics module.

    Depending on the type of concrete object that is passed, the returned
    dictionary of features could have one or multiple records/rows.

    Params
    ------
    pyrad_factory: PyradFactory
        A concrete instantiation of the abstract class Pyrad. This is usually
        created with Pyrad.factory()

    Returns
    -------
    e_code: int
        0 if no issues
    features: OrderedDict or sitk.Image
        dictionary containing calculated signature ("<imageType>_<featureClass>_<featureName>":value).
        In case of segment-based extraction, value type for features is float.
        If voxel-based is selected, type is a SimpleITK image of the parameter
        map instead of a float value for each feature. Type of diagnostic
        features differs, but can always be represented as a string.
    """
    log.info("This is the beginning of the run file")
    features = pyrad_factory.calc_radiomics()

    return 0, features
