"""Module for calculating radiomic features using PyRadiomics."""
import logging
import os
from abc import ABC, abstractmethod
from collections import OrderedDict
from dataclasses import dataclass

import SimpleITK as sitk
from radiomics import featureextractor

log = logging.getLogger(__name__)


def get_pyrad_calc(pyrad_calc_name: str):
    """Create a dictionary of all calculators to quickly get the corresponding calculator."""
    calculators = {
        VoxelPyradCalc.pyrad_calc_name: VoxelPyradCalc,
        RadPyradCalc.pyrad_calc_name: RadPyradCalc,
    }
    pyrad_calc = calculators.get(pyrad_calc_name)
    if pyrad_calc is None:
        raise ValueError(
            "Could not find subclass of class %s named %s. Could not "
            "create instance.\n"
            "Available names: \n\t%s"
            % (PyradCalc.__name__, pyrad_calc_name, list(calculators.keys()))
        )
    else:
        return pyrad_calc


class PyradCalc(ABC):
    """Abstract class for calculating radiomic features."""

    @dataclass
    class PyradCalcParams:
        """Dataclass for Pyradiomics calculator parameters."""

        pyrad_calc_name: str
        pyrad_calc_name: str
        mask_passed: bool
        path_image: (str, os.PathLike)
        path_mask: (str, os.PathLike)
        path_params: (str, os.PathLike, None)

    @classmethod
    def factory(cls, params: PyradCalcParams):
        """Instantiate and return the pyrad calculator."""
        return get_pyrad_calc(pyrad_calc_name=params.pyrad_calc_name)(
            mask_passed=params.mask_passed,
            path_image=params.path_image,
            path_mask=params.path_mask,
            path_params=params.path_params,
        )

    def __init__(
        self,
        mask_passed: bool,
        path_image: (str, os.PathLike),
        path_mask: (str, os.PathLike),
        path_params: (str, os.PathLike, None) = None,
    ):
        """Initialize the instance's attributes."""
        self.mask_passed = mask_passed
        self.path_image = path_image
        self.path_mask = path_mask
        self.path_params = path_params

    @property
    @abstractmethod
    def pyrad_calc_name(self):
        """The name of the pyrad calculator object to use."""

    @abstractmethod
    def calc_radiomics(self, image_sitk: sitk.Image, mask_sitk: sitk.Image):
        """Calculate the radiomic features."""
        pass

    @abstractmethod
    def create_extractor(self, image_sitk: sitk.Image):
        """Abstract method for creating the feature extractor."""

    @staticmethod
    def check_image_dimensions(image_sitk: sitk.Image, mask_sitk: sitk.Image):
        """Verifies that both images have the same value."""
        if image_sitk.GetSize() != mask_sitk.GetSize():
            raise AttributeError(
                "Size mismatch: the input image is size '%s' and its mask is "
                "'%s'. Sizes must be the same."
                % (str(image_sitk.GetSize()), str(mask_sitk.GetSize()))
            )


class VoxelPyradCalc(PyradCalc):
    """A radiomics calculator for 3D images. This calculator calculates features."""

    pyrad_calc_name = "voxel_rad_calc"

    def create_extractor(self, image_sitk: sitk.Image):
        """Creates and returns extractor for processing 2D and 3D images."""
        pass

    def calc_radiomics(self, image_sitk: sitk.Image, mask_sitk: sitk.Image):
        """Calculate radiomic features for 3D images."""
        pass


class RadPyradCalc(PyradCalc):
    """A radiomics calculator for 2D and 3D images.

    Though each image slice in the 3D image will get its own 2D feature calculations. See VoxelPyradCalc
    for voxel-based calculations.
    """

    pyrad_calc_name = "rad_calc"

    def create_extractor(self, image_sitk: sitk.Image):
        """Creates and returns extractor for processing 2D and 3D images."""
        if self.path_params:
            log.info("Loading parameters from %s" % self.path_params)
            extractor = featureextractor.RadiomicsFeatureExtractor(self.path_params)
        else:
            extractor = featureextractor.RadiomicsFeatureExtractor()

        # Add shape2D feature extraction if a valid mask was passed
        if self.mask_passed:
            log.info("Enabling shape2D feature since mask was passed and image is 2D.")
            extractor.enableFeatureClassByName("shape", enabled=False)
            extractor.enableFeatureClassByName("shape2D")
        else:
            log.info(
                "Disabling shape feature since mask was created using"
                "the entire image."
            )
            extractor.enableFeatureClassByName("shape", enabled=False)

        log.info("Extraction parameters:\n\t%s" % extractor.settings)
        log.info("\nEnabled filters:\n\t%s" % extractor.enabledImagetypes)
        log.info("\nEnabled features:\n\t%s" % extractor.enabledFeatures)

        return extractor

    @staticmethod
    def _calc_radiomics(
        extractor, image_sitk, mask_sitk, path_image, path_mask
    ) -> OrderedDict:
        """Calculate 2D radiomic features for a 2D/3D image."""
        log.info(
            "\tCalculating radiomic features for image of size %s"
            % str(image_sitk.GetSize())
        )
        # Calculate radiomic features. Returns an OrderedDict
        features = extractor.execute(image_sitk, mask_sitk)

        # Add the image and mask paths to an output dictionary, and then update
        # this with the features dict
        dict_out = OrderedDict({"path_image": path_image, "path_mask": path_mask})
        dict_out.update(features)
        return dict_out

    def calc_radiomics(
        self, image_sitk: sitk.Image, mask_sitk: sitk.Image
    ) -> OrderedDict:
        """Calculate radiomic features for 2D and 3D arrays.

        Returns an OrderedDict of features, each row representing features
        for each 2D image in the 3D array.
        """
        if not isinstance(image_sitk, sitk.Image):
            raise AttributeError(
                "Wrong type for image_sitk. Expected SimpleITK.Image, got %s"
                % type(image_sitk)
            )

        if image_sitk.GetDimension() not in [2, 3]:
            raise AttributeError(
                "Can only calculate 2D and 3D images, got dimension '%s'"
                % image_sitk.GetDimension()
            )

        self.check_image_dimensions(image_sitk=image_sitk, mask_sitk=mask_sitk)
        extractor = self.create_extractor(image_sitk=image_sitk)

        log.info(
            "Calculating 2D features for 2D/3D image of size %s."
            % str(image_sitk.GetSize())
        )

        return self._calc_radiomics(
            extractor=extractor,
            image_sitk=image_sitk,
            mask_sitk=mask_sitk,
            path_image=self.path_image,
            path_mask=self.path_mask,
        )
