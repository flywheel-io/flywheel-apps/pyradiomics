"""Utility functions for the PyRadiomics gear."""
import yaml

# set of allowed feature names
allowed_features = {
    "Autocorrelation",
    "JointAverage",
    "ClusterProminence",
    "ClusterShade",
    "ClusterTendency",
    "Contrast",
    "Correlation",
    "DifferenceAverage",
    "DifferenceEntropy",
    "DifferenceVariance",
    "JointEnergy",
    "JointEntropy",
    "Imc1",
    "Imc2",
    "Idm",
    "Idmn",
    "Id",
    "Idn",
    "InverseVariance",
    "MaximumProbability",
    "SumEntropy",
    "SumSquares",
}


def load_parameters(file_path):
    """Load and parse the YAML parameters file."""
    try:
        with open(file_path, "r") as f:
            return yaml.safe_load(f)
    except yaml.YAMLError as e:
        print(f"Error loading parameters file: {e}")
        return None


def validate_required_sections(parameters):
    """Check if all required sections exist."""
    required_sections = ["setting", "imageType", "featureClass"]
    missing_sections = [
        section for section in required_sections if section not in parameters
    ]
    if missing_sections:
        print(f"Missing sections: {', '.join(missing_sections)}")
        return False
    return True


def validate_setting(setting):
    """Check specific constraints for the 'setting' section."""
    if any(value == 0 for value in setting.values()):
        print("Invalid value for setting: 0 is not allowed")
        return False
    return True


def validate_image_type(image_type):
    """Check specific constraints for the 'imageType' section."""
    if any(settings is None for settings in image_type.values()):
        print("Invalid value for image type: None is not allowed")
        return False
    return True


def validate_feature_class(feature_class):
    """Check specific constraints for the 'featureClass' section."""
    allowed_features = {"circle", "square", "triangle"}  # example allowed features
    for feature_class, features in feature_class.items():
        if features is not None and not isinstance(features, list):
            print(
                "Invalid value for feature class: Features must be specified as a list"
            )
            return False

        if features and feature_class == "shape":
            for feature in features:
                if feature not in allowed_features:
                    print(
                        f"Invalid feature '{feature}' specified in {feature_class}: Not in the list of allowed features"
                    )
                    return False
    return True


def validate_parameters_file(file_path):
    """Param file validator."""
    parameters = load_parameters(file_path)
    if parameters is None:
        return False

    if not validate_required_sections(parameters):
        return False

    if not validate_setting(parameters.get("setting", {})):
        return False

    if not validate_image_type(parameters.get("imageType", {})):
        return False

    if not validate_feature_class(parameters.get("featureClass", {})):
        return False

    return True
