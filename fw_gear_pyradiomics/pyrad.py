"""Module for creating objects for use with pyradiomics."""

import logging
import os
from collections import OrderedDict
from dataclasses import dataclass

import SimpleITK as sitk

from fw_gear_pyradiomics.imgprep import EmptyMaskImagePreparer, ImagePreparer
from fw_gear_pyradiomics.imgreaders import ImageReader
from fw_gear_pyradiomics.pyradcalc import PyradCalc

log = logging.getLogger(__name__)

"""
SimpleITK might be able to handle dicoms...
class DicomPyrad(Pyrad):

    file_ext = ".dcm"

    pass

"""


def read_image(image_reader, image_type):
    """Reads an image with the given image reader."""
    try:
        return image_reader.read_image()
    except Exception as e:
        log.error(f"Error loading image of type {image_type}, with exception:")
        log.exception(e)


@dataclass
class PyradFactoryParams:
    """Dataclass for Pyradiomics factory parameters."""

    path_image: (str, os.PathLike)
    path_mask: (str, os.PathLike, None)
    output_dir: (str, os.PathLike)
    config_dict: dict
    path_params: (str, os.PathLike, None) = None
    mask_passed: bool = False


class PyradFactory:
    """Factory for creating Pyradiomics objects."""

    def __init__(self, params: PyradFactoryParams):
        """Creates the pyradiomics factory to handle different types of images and options.

        Params
        ------
        config_dict: dict
            A configuration dict with the following parameters
        voxel_based: bool
            Whether to calculate voxel-based radiomic features.
        """
        self.params = params
        self.path_image = params.path_image
        if params.path_mask is None:
            params.path_mask = ""
        self.path_mask = params.path_mask
        self.output_dir = params.output_dir
        self.config_dict = params.config_dict
        self.path_params = params.path_params
        self.file_name = os.path.splitext(os.path.split(self.path_image)[-1])[
            0
        ]  # extension and directory removed

        # Create our input image mask readers
        self.image_reader = ImageReader.factory(path_image=params.path_image)
        self.image_reader_mask = ImageReader.factory(path_image=params.path_mask)

        # Create our image preparers
        self.image_preparer = ImagePreparer.factory(
            image_prep_name=self.image_reader.image_prep_name,
            config_dict=params.config_dict,
        )
        self.image_preparer_mask = ImagePreparer.factory(
            image_prep_name=self.image_reader_mask.image_prep_name,
            config_dict=params.config_dict,
        )

        # Create our radiomics calculator
        if self.config_dict["voxel_based"]:
            params.pyrad_calc_name = "voxel_rad_calc"
        else:
            params.pyrad_calc_name = "rad_calc"

        if isinstance(self.image_preparer_mask, EmptyMaskImagePreparer):
            self.mask_passed = False
        else:
            self.mask_passed = True

        self.pyrad_calc = PyradCalc.factory(params)

    def calc_radiomics(self) -> (OrderedDict, sitk.Image):
        """The template method for calculating pyradiomics features.

        It returns an OrderedDict of features or a SimpleITK image if voxel-based option
        was selected.

        Returns
        -------
        features: OrderedDict or sitk.Image
            dictionary containing calculated signature ("<imageType>_<featureClass>_<featureName>":value).
            In case of segment-based extraction, value type for features is float.
            If voxel-based is selected, type is a SimpleITK image of the parameter
            map instead of a float value for each feature. Type of diagnostic
            features differs, but can always be represented as a string.
        """
        # Get the input image and its mask

        image_sitk = read_image(self.image_reader, "Image")
        mask_sitk = read_image(self.image_reader_mask, "Mask")

        # Prepare both images. pass the image_sitk to create a mask if the
        # preparer is EmptyMaskImagePreparer
        image_sitk = self.image_preparer.prepare_image(image_sitk=image_sitk)
        if isinstance(self.image_preparer_mask, EmptyMaskImagePreparer):
            mask_sitk = self.image_preparer_mask.prepare_image(image_sitk=image_sitk)
        elif image_sitk is None:
            log.error("Image is empty.")
            return None
        else:
            mask_sitk = self.image_preparer_mask.prepare_image(image_sitk=mask_sitk)

        # Calculate radiomic features
        if not isinstance(image_sitk, sitk.Image) and not isinstance(
            mask_sitk, sitk.Image
        ):
            log.error("Image and mask must be SimpleITK images.")
            return None

        features = self.pyrad_calc.calc_radiomics(
            image_sitk=image_sitk, mask_sitk=mask_sitk
        )
        return features
